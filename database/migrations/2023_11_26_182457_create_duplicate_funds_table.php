<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('duplicate_funds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('fund_a_id');
            $table->foreign('fund_a_id')->references('id')->on('funds');
            $table->unsignedBigInteger('fund_b_id');
            $table->foreign('fund_b_id')->references('id')->on('funds');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('duplicate_funds');
    }
};
