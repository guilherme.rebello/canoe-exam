## Canoe Exam

This repo is my submission of the Canoe exam

## Routes

GET /funds returns all funds in db

possible filters: name, manager, year

POST /funds create a new fund

PUT /funds/{fund} update a fund

GET /funds/duplicates returns all duplicated funds

## Tests

All routes are tested in tests folder
