<?php

use App\Http\Controllers\DuplicateFundsController;
use App\Http\Controllers\FundController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/funds', [FundController::class, 'index']);
Route::post('/funds', [FundController::class, 'store']);
Route::put('/funds/{fund}', [FundController::class, 'update']);
Route::get('/funds/duplicates', DuplicateFundsController::class);
