<?php

namespace Tests\Feature;

use App\Models\DuplicateFund;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DuplicateFundsControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function test_should_return_a_list_with_duplicate_funds(): void
    {
        DuplicateFund::factory()->create();

        $response = $this->get('/funds/duplicates');

        $response->assertStatus(200);
    }
}
