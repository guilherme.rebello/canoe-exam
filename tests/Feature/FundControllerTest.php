<?php

namespace Tests\Feature;

use App\Events\DuplicateFundWarningEvent;
use App\Models\Alias;
use App\Models\Company;
use App\Models\Fund;
use App\Models\Manager;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class FundControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_show_all_funds(): void
    {
        /** @var Fund $fund */
        $fund = Fund::factory()
                ->has(Alias::factory()->count(3), 'aliases')
                ->has(Company::factory()->count(2), 'companies')
                ->create();

        $response = $this->get('/funds');

        $response->assertJson([
            'data' => [
                [
                    'name' => $fund->name,
                    'year' => $fund->year,
                    'manager' => [
                        'name' => $fund->manager->name
                    ],
                    'aliases' => [],
                    'companies' => []
                ]
            ]
        ]);

        $response->assertStatus(200);
    }

    public function test_show_all_funds_filtered_by_name(): void
    {
        /** @var Fund $fund */
        $fund = Fund::factory(['name' => 'test'])->create();

        $fundAlias = Fund::factory()
            ->has(Alias::factory(['name' => 'test 2']), 'aliases')
            ->create();

        $response = $this->get('/funds?name=test');

        $response->assertJson([
            'data' => [
                [
                    'name' => $fund->name,
                    'year' => $fund->year,
                    'manager' => [
                        'name' => $fund->manager->name
                    ],
                    'aliases' => [],
                    'companies' => []
                ]
            ]
        ]);

        $response->assertStatus(200);

        $response = $this->get('/funds?name=test 2');

        $response->assertJson([
            'data' => [
                [
                    'name' => $fundAlias->name,
                    'year' => $fundAlias->year,
                    'manager' => [
                        'name' => $fundAlias->manager->name
                    ],
                    'aliases' => [],
                    'companies' => []
                ]
            ]
        ]);

        $response->assertStatus(200);
    }

    public function test_show_all_funds_filtered_by_manager(): void
    {
        /** @var Fund $fund */
        $fund = Fund::factory()->create();

        Fund::factory()->create();

        $response = $this->get('/funds?manager='.$fund->manager->name);

        $response->assertJson([
            'data' => [
                [
                    'name' => $fund->name,
                    'year' => $fund->year,
                    'manager' => [
                        'name' => $fund->manager->name
                    ],
                    'aliases' => [],
                    'companies' => []
                ]
            ]
        ]);

        $response->assertStatus(200);
    }

    public function test_show_all_funds_filtered_by_year(): void
    {
        /** @var Fund $fund */
        $fund = Fund::factory(['year' => 2000])->create();

        Fund::factory()->create();

        $response = $this->get('/funds?year=2000');

        $response->assertJson([
            'data' => [
                [
                    'name' => $fund->name,
                    'year' => $fund->year,
                    'manager' => [
                        'name' => $fund->manager->name
                    ],
                    'aliases' => [],
                    'companies' => []
                ]
            ]
        ]);

        $response->assertStatus(200);
    }

    public function test_store_a_new_fund(): void
    {
        $body = [
            'name' => 'test',
            'year' => 2000,
            'manager' => 'Manager Test',
            'companies' => [
                [
                    'name' => 'Company 1'
                ],
                [
                    'name' => 'Company 2'
                ]
            ]
        ];

        $response = $this->post('/funds', $body);

        $response->assertStatus(201);

        $this->assertDatabaseHas('funds', [
            'name' => $body['name'],
            'year' => $body['year'],
        ]);

        $this->assertDatabaseHas('managers', [
            'name' => $body['manager']
        ]);

        $this->assertDatabaseHas('companies', [
            'name' => $body['companies'][0]['name']
        ]);

        $this->assertDatabaseHas('companies', [
            'name' => $body['companies'][1]['name']
        ]);
    }

    public function test_update_a_fund()
    {
        /** @var Fund $fund */
        $fund = Fund::factory()->create();

        $body = [
            'name' => 'test',
            'year' => 2000,
            'manager' => 'Manager Test',
            'companies' => [
                [
                    'name' => 'Company 1'
                ],
                [
                    'name' => 'Company 2'
                ]
            ],
            'aliases' => [
                [
                    'name' => 'Fund alias'
                ]
            ]
        ];

        $response = $this->put('/funds/'.$fund->id, $body);

        $response->assertStatus(200);

        $this->assertDatabaseHas('funds', [
            'name' => $body['name'],
            'year' => $body['year'],
        ]);

        $this->assertDatabaseHas('managers', [
            'name' => $body['manager']
        ]);

        $this->assertDatabaseHas('companies', [
            'name' => $body['companies'][0]['name']
        ]);

        $this->assertDatabaseHas('companies', [
            'name' => $body['companies'][1]['name']
        ]);

        $this->assertDatabaseHas('aliases', [
            'name' => $body['aliases'][0]['name']
        ]);
    }

    public function test_should_send_duplicate_event_on_updating_fund()
    {
        Event::fake();

        $body = [
            'name' => 'test',
            'year' => 2000,
        ];

        $manager = Manager::factory()->create();

        /** @var Fund $fund */
        $fund = Fund::factory([
            'manager_id' => $manager->id
        ])->create();

        Fund::factory([
            'manager_id' => $manager->id,
            'name' => 'test',
            'year' => 2000,
        ])->create();

        $this->put('/funds/'.$fund->id, $body);

        Event::assertDispatched(DuplicateFundWarningEvent::class);
    }

    public function test_should_flag_duplicate_funds()
    {
        $body = [
            'name' => 'test',
            'year' => 2000,
        ];

        $manager = Manager::factory()->create();

        /** @var Fund $fund */
        $fund = Fund::factory([
            'manager_id' => $manager->id
        ])->create();

        $fundToUpdate = Fund::factory([
            'manager_id' => $manager->id,
            'name' => 'test',
            'year' => 2000,
        ])->create();

        $this->put('/funds/'.$fund->id, $body);

        $this->assertDatabaseHas('duplicate_funds', [
            'fund_b_id' => $fundToUpdate->id,
            'fund_a_id' => $fund->id
        ]);
    }
}
