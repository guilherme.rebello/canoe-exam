<?php

namespace App\Services;

use App\Events\DuplicateFundWarningEvent;
use App\Models\DuplicateFund;
use App\Models\Fund;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class FundService
{

    public function __construct(
        private ManagerService $managerService,
        private CompanyService $companyService,
        private AliasService $aliasService,
    )
    {
    }

    public function find(Request $request): Collection
    {
        $query = Fund::query()->select('funds.*');

        if ($request->has('name')) {
            $query->leftJoin('aliases', 'funds.id', '=', 'aliases.fund_id');
            $query->where(function (Builder $query) use ($request) {
                $query->where('funds.name', $request->get('name'))
                    ->orWhere('aliases.name', $request->get('name'));
            });
        }

        if ($request->has('manager')) {
            $query->leftJoin('managers', 'funds.manager_id', '=', 'managers.id');
            $query->where('managers.name', $request->get('manager'));
        }

        if ($request->has('year')) {
            $query->where('funds.year', $request->get('year'));
        }

        return $query->get();
    }

    public function create(Request $request): Fund
    {
        $fund = new Fund();

        $manager = $this->managerService->findOrCreate($request->get('manager'));

        $fund->fill($request->all());
        $fund->manager_id = $manager->id;
        $fund->save();

        $this->addCompanies($request->get('companies'), $fund);

        $this->checkIfFundIsDuplicated($fund);

        return $fund;
    }

    public function update(Fund $fund, Request $request)
    {
        $fund->fill($request->all());

        if ($request->has('manager')) {
            $manager = $this->managerService->findOrCreate($request->get('manager'));
            $fund->manager_id = $manager->id;
        }

        if ($request->has('companies')) {
            $fund->companies()->detach();
            $this->addCompanies($request->get('companies'), $fund);
        }

        if ($request->has('aliases')) {
            $fund->aliases()->delete();

            $aliases = $request->get('aliases');

            foreach ($aliases as $alias) {
                $this->aliasService->findOrCreate($alias['name'], $fund);
            }
        }

        $fund->save();

        $this->checkIfFundIsDuplicated($fund);

        return $fund;
    }

    private function addCompanies(array $companies, Fund $fund): void
    {
        foreach ($companies as $company) {
            $companyModel = $this->companyService->findOrCreate($company['name']);

            $fund->companies()->attach($companyModel);
        }
    }

    private function findSimilarFund(Fund $fund): Model|Builder|null
    {
        $query = Fund::query()->select('funds.*')
            ->leftJoin('aliases', 'funds.id', '=', 'aliases.fund_id')
            ->leftJoin('managers', 'funds.manager_id', '=', 'managers.id')
            ->where(function (Builder $query) use ($fund) {
                $query->where('funds.name', $fund->name)
                    ->orWhere('aliases.name', $fund->name);
            })
            ->where('managers.name', $fund->manager->name)
            ->where('funds.id', '<>', $fund->id);

        return $query->first();
    }

    private function checkIfFundIsDuplicated(Fund $fund): void
    {
        $duplicatedFund = $this->findSimilarFund($fund);

        if ($duplicatedFund) {
            DuplicateFundWarningEvent::dispatch($fund, $duplicatedFund);
        }
    }

    public function addPossibleDuplicateFunds(Fund $newFund, Fund $duplicateFund): DuplicateFund
    {
        $duplicateFundModel = new DuplicateFund();

        $duplicateFundModel->fund_a_id = $newFund->id;
        $duplicateFundModel->fund_b_id = $duplicateFund->id;

        $duplicateFundModel->save();

        return $duplicateFundModel;
    }

    public function getDuplicates(): LengthAwarePaginator
    {
        return DuplicateFund::query()->paginate();
    }
}
