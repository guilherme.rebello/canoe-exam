<?php

namespace App\Services;

use App\Models\Manager;

class ManagerService
{

    public function findOrCreate(string $name): Manager
    {
        $manager = Manager::query()->where('name', $name)->first();

        if (!$manager) {
            $manager = new Manager();
            $manager->name = $name;
            $manager->save();
        }

        return $manager;
    }
}
