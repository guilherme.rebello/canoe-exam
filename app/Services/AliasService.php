<?php

namespace App\Services;

use App\Models\Alias;
use App\Models\Fund;

class AliasService
{
    public function findOrCreate(string $name, Fund $fund): Alias
    {
        $alias = Alias::query()->where('name', $name)->first();

        if (!$alias) {
            $alias = new Alias();
            $alias->name = $name;
            $alias->fund_id = $fund->id;
            $alias->save();
        }

        return $alias;
    }
}
