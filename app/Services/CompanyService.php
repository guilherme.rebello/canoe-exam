<?php

namespace App\Services;

use App\Models\Company;

class CompanyService
{

    public function findOrCreate(mixed $name): Company
    {
        $company = Company::query()->where('name', $name)->first();

        if (!$company) {
            $company = new Company();
            $company->name = $name;
            $company->save();
        }

        return $company;
    }
}
