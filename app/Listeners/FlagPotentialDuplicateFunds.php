<?php

namespace App\Listeners;

use App\Events\DuplicateFundWarningEvent;
use App\Services\FundService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class FlagPotentialDuplicateFunds
{
    /**
     * Create the event listener.
     */
    public function __construct(private FundService $fundService)
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(DuplicateFundWarningEvent $event): void
    {
        $this->fundService->addPossibleDuplicateFunds($event->newFund, $event->duplicateFund);
    }
}
