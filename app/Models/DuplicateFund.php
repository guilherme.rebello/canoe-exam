<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class DuplicateFund extends Model
{
    use HasFactory;

    protected $fillable = ['fund_a_id', 'fund_b_id'];

    public function fundA(): HasOne
    {
        return $this->hasOne(Fund::class, 'id','fund_a_id');
    }

    public function fundB(): HasOne
    {
        return $this->hasOne(Fund::class,'id', 'fund_b_id');
    }
}
