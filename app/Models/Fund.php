<?php

namespace App\Models;

use App\Observers\FundObserver;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string name
 * @property int year
 * @property int manager_id
 * @property Manager manager
 * @property Collection aliases
 * @property Collection companies
 */
class Fund extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'year', 'manager_id'];

    public function manager(): BelongsTo
    {
        return $this->belongsTo(Manager::class);
    }

    public function aliases(): HasMany
    {
        return $this->hasMany(Alias::class);
    }

    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class);
    }
}
