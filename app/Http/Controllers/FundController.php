<?php

namespace App\Http\Controllers;

use App\Http\Resources\FundResource;
use App\Models\Fund;
use App\Services\FundService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FundController extends Controller
{
    public function __construct(private FundService $fundService)
    {
    }

    public function index(Request $request): JsonResponse
    {

        $fund = $this->fundService->find($request);

        return response()->json(['data' => FundResource::collection($fund)]);
    }

    public function store(Request $request): JsonResponse
    {
        $fund = $this->fundService->create($request);

        return response()->json(['data' => FundResource::make($fund)], 201);
    }

    public function update(Fund $fund, Request $request)
    {
        $updatedFund = $this->fundService->update($fund, $request);

        return response()->json(['data' => FundResource::make($updatedFund)]);
    }
}
