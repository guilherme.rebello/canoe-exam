<?php

namespace App\Http\Controllers;

use App\Http\Resources\DuplicateFundResource;
use App\Services\FundService;
use Illuminate\Http\Request;

class DuplicateFundsController extends Controller
{

    public function __construct(private FundService $fundService)
    {
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $duplicates = $this->fundService->getDuplicates();

        return response()->json(['data' => DuplicateFundResource::collection($duplicates)]);
    }
}
