<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FundResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'name' => $this->name,
            'year' => $this->year,
            'manager' => ManagerResource::make($this->manager),
            'aliases' => AliasResource::collection($this->aliases),
            'companies' => CompanyResource::collection($this->companies),
        ];
    }
}
